#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2012-2014 Roger Pereira Boff <roger@gmail.com>
# All rights reserved
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., or visit: http://www.gnu.org/.
#
# Author: Roger Pereira Boff <rogerboff@gmail.com>
#
# Arquivo: C:/Users/alemaum/workspace/suporte/sat/admin.py
#
# based:
# http://blog.richard.do/index.php/2013/04/setting-up-nginx-django-uwsgi-a-tutorial-that-actually-works/
# https://gist.github.com/postrational/5747293#file-gunicorn_start-bash
# http://michal.karzynski.pl/blog/2013/10/29/serving-multiple-django-applications-with-nginx-gunicorn-supervisor/
# http://goodcode.io/blog/django-nginx-gunicorn/
#

# from pydev import pydevd
# pydevd.settrace('192.168.2.101', port=9876, stdoutToServer=True, stderrToServer=True)

import argparse
import os
import re
import sys
import virtualenv

from random import randint
from os.path import exists as pexists, sep as psep, islink as pislink, join as pjoin
from os import mkdir as omkdir, walk as owalk, remove as oremove, rmdir as ormdir

__author__ = 'Roger Pereira Boff <rogerboff@gmail.com>'
__version__ = '0.1.0'


class ImplementedError(RuntimeError):
    """ Metodo ou função implementada incorretamente. """
    pass


def msg_erro(msg):
    msg = 'Error: {}\n'.format(msg)
    sys.stderr.write(msg)
    sys.exit(1)


class NginxDjangoGunicorn:
    def __init__(self):
        self.valida_dominio = re.compile(r'^([0-9a-z][-\w]*[0-9a-z]\.)+[a-z0-9\-]{2,15}$')
        self.dominios = []
        self.remover = None

    def __argumentos(self):
        parser = argparse.ArgumentParser(description='Gera estruturas necessária para hospedar páginas com Django + '
                                                     'Nginx + Virtualenv + Gunicorn')
        parser.add_argument('--projeto',
                            '-p',
                            dest='projeto',
                            required=True,
                            help='Nome do projeto que será hospedado. [Obrigatório]')
        parser.add_argument('--dominio',
                            '-d',
                            required=True,
                            help='Nome do dominio a ser hospedado [Obrigatório]')
        parser.add_argument('--dominios',
                            nargs='+',
                            help='Nome dos dominios adicionais a serem respondidos')
        parser.add_argument('--ip',
                            default='127.0.0.1',
                            help='IP que o Gunicorn deverá responder. [Padrão: 127.0.0.1')
        parser.add_argument('--porta',
                            default=randint(58000, 59999),
                            help='Porta a ser utilizado pelo Gunicorn. Caso não seja informada a porta, será usada'
                                 'uma porta randonica entre as 58000:59999')
        parser.add_argument('--atualizar',
                            action='store_true',
                            default=False,
                            help='Efetua a atualização dos arquivos de configuração, atualizado os dominios e porta'
                                 'da hospedagem já existente.')
        parser.add_argument('--caminho',
                            action='store',
                            default='/var/www',
                            help='Informa o caminho onde deve ser criado o projeto hospedado. Padrão "/var/www"')
        parser.add_argument('--proteger',
                            action='store_true',
                            default=False,
                            help='Adiciona trava contra a remoção do projeto hospedado pelo comando --remover')
        parser.add_argument('--remover',
                            action='store_true',
                            default=False,
                            help='Remove o projeto caso o mesmo já exista, permitindo ser criado outro projeto com'
                                 'o mesmo nome')
        parser.add_argument('--remover-forcado',
                            action='store_true',
                            default=False,
                            help='Solicita somente uma vez a confirmação para remoção do projeto. Necessita de'
                                 '--remover.')
        self.args = parser.parse_args()

    def __valida_dominio(self):
        if not bool(self.valida_dominio.match(self.args.dominio)):
            msg_erro('O dominio "{}" não é um dominio válido.'.format(self.args.dominio))
        else:
            self.dominios.append(self.args.dominio)

        if self.args.dominios:
            for dominio in self.args.dominios:
                if not bool(self.valida_dominio.match(dominio)) and dominio != 'localhost':
                    msg_erro('O dominio "{}" não é um dominio válido.'.format(dominio))
                else:
                    self.dominios.append(dominio)

    def __define_ambiente(self):
        self.__valida_dominio()

        self.projeto = self.args.projeto
        self.dominio = self.args.dominio
        self.dominios = ' '.join(self.dominios)
        self.base_projeto = self.args.caminho + psep + self.dominio
        self.ip = self.args.ip
        self.porta = str(self.args.porta)

        self.diretorios = {}
        for d in ('venv', 'conf', 'src', 'logs', 'run', pjoin('src', self.projeto)):
            self.diretorios[d] = pjoin(self.base_projeto, d)

        self.confs = {}
        for c in ('nginx', 'supervisor', 'celeryd'):
            self.confs[c] = pjoin(self.diretorios['conf'], ('%s.conf' % c))

        self.atualizar = self.args.atualizar
        self.remover = self.args.remover
        self.forcado = self.args.remover_forcado
        self.proteger = self.args.proteger

        self.aCriado = self.base_projeto + psep + '.create'
        self.aProtege = self.base_projeto + psep + '.protect'

    def __imprime_configuracoes(self):
        print('Criado o projeto "{projeto}" com sucesso.'
              '\n'
              'Dominio principal       : {dominio}\n'
              'Dominio(s) respondido(s): {dominios}\n'
              'Caminho do dominio      : {base_projeto}\n'
              'Caminho do projeto      : {base_dir}src{sep}{projeto}\n'
              'Caminho src             : {src}\n'
              'Caminho venv            : {venv}\n'
              'Caminho conf            : {conf}\n'
              'Caminho log             : {logs}\n'
              # 'Gateway                 : {gateway}\n'
              .format(projeto=self.projeto,
                      base_projeto=self.base_projeto,
                      # gateway=self.ip + ':' + self.porta,
                      dominio=self.dominio,
                      dominios=self.dominios,
                      base_dir=self.base_projeto + psep,
                      src=self.diretorios['src'],
                      venv=self.diretorios['venv'],
                      conf=self.diretorios['conf'],
                      logs=self.diretorios['logs'],
                      sep=psep))

    def __pergunta(self, pergunta, **kwargs):
        opcoes = {}
        perguntas = []
        if 'opcoes' in kwargs:
            if len(kwargs['opcoes']) > 1:
                perguntas = kwargs['opcoes'].keys()
                for opcao in kwargs['opcoes']:
                    opcoes[opcao.lower()] = kwargs['opcoes'][opcao]
                    opcoes[opcao.lower()]['padrao'] = opcao
            else:
                raise AttributeError('O atributo "opcoes" deve ter no mínimo de duas opções')

        while True:
            resposta = raw_input('{pergunta} [{opcoes}]: '.format(pergunta=pergunta, opcoes='/'.join(perguntas)))
            if resposta.lower() in opcoes:
                questao = opcoes[resposta.lower()]
                if 'case' in opcoes[resposta.lower()]:
                    if resposta != questao['padrao']:
                        print('Responda "{}" e não "{}" para continuar.'.format(questao['padrao'], resposta))
                        continue

                if 'msg' in questao:
                    print(questao['msg'])

                if 'return' in questao:
                    return questao['return']
                elif 'callback' in questao:
                    questao['callback']()
                else:
                    raise ImplementedError('Não localizei nenhum retorno para a opção de retorno ou callback.')
            else:
                print('Opção inválida.')

    def __remover_projeto(self):
        if self.remover:

            if pexists(self.aProtege):
                print('Remover primeiro o arquivo "%s" para poder apagar esse site' % self.aProtege)
                sys.exit(1)

            if not pexists(self.aCriado):
                print('Não é possível remover esse site, pois ele não foi criado pela aplicação.\n'
                      'Para remover o site, crie o arquivo "%s"' % self.aCriado)
                sys.exit(1)

            opcoes = {
                'Sim': {'case': True, 'return': True},
                'Nao': {'callback': sys.exit, 'msg': 'Processo cancelado'}
            }
            if self.forcado:
                pergunta = 'Você deseja apagar o diretório "%s"? Esse processo é irreversível!' % self.base_projeto
                if self.__pergunta(pergunta, opcoes=opcoes):
                    pass
            else:
                pergunta = 'Você deseja apagar o diretório "%s"?' % self.base_projeto
                if self.__pergunta(pergunta, opcoes=opcoes):
                    pergunta = 'Você tem certeza que deseja apagar o diretório "%s"?' % self.base_projeto
                    if self.__pergunta(pergunta, opcoes=opcoes):
                        pergunta = 'Você confirma que deseja apagar o diretório, pois esse processo é irreversível'
                        if self.__pergunta(pergunta, opcoes=opcoes):
                            pass
            try:
                sys.stdout.write('Apagando o diretório : ')
                for root, dirs, files in owalk(self.base_projeto, topdown=False):
                    for name in files:
                        name = pjoin(root, name)
                        oremove(name)

                    for name in dirs:
                        name = pjoin(root, name)
                        if not pislink(name):
                            ormdir(name)
                        else:
                            oremove(name)

                ormdir(self.base_projeto)

                sys.stdout.write('[OK]\n')
            except:
                sys.stdout.write('[FALHA]\n')
                raise
        else:
            # Sobre um raise pois esse metodo não deve ser chamado a não ser pelo metodo __cria_ambiente()
            raise ImplementedError('Esse metodo não deveria ter sido chamado nesse contexto.')

    def __cria_diretorios(self):
        if pexists(self.base_projeto):
            if self.atualizar:
                return
            if self.remover:
                self.__remover_projeto()
            else:
                msg_erro('O diretório já existe: {dir}'.format(dir=self.base_projeto))

        if pexists(os.path.dirname(self.base_projeto)) is False:
            omkdir(os.path.dirname(self.base_projeto))
        omkdir(self.base_projeto)

        try:
            sys.stdout.write('Criando os diretórios: ')
            for d in self.diretorios.values():
                omkdir(d)

            with open(self.aCriado, 'w+') as arq:
                arq.write('create')
                arq.close()

            if self.proteger:
                with open(self.aProtege, 'w+') as arq:
                    arq.write('protect')
                    arq.close()
            sys.stdout.write('[OK]\n')
        except:
            sys.stdout.write('[FALHA]\n')
            raise

    def __cria_virtualenv(self):
        sys.stdout.write('Criando virtualenv   : ')
        try:
            virtualenv.create_environment(self.diretorios['venv'],
                                          site_packages=True)
            sys.stdout.write('[OK]\n')
        except:
            sys.stdout.write('[FALHA]\n')
            raise

    def __cria_conf_nginx_uwsgi(self):
        yield 'server {'
        yield '    listen 80;'
        yield '    server_name %s;' % self.dominios
        yield '    root %s/src/%s;' % (self.base_projeto, self.projeto)
        yield '    access_log %s/logs/access.log;' % self.base_projeto
        yield '    error_log %s/logs/error.log;' % self.base_projeto
        yield ''
        yield '    location /static/ { # STATIC_URL'
        yield '        alias %s/src/static/; # STATIC_ROOT' % self.base_projeto
        yield '        expires 30d;'
        yield '    }'
        yield ''
        yield '    location /media/ { # MEDIA_URL'
        yield '        alias %s/src/media/; # MEDIA_ROOT' % self.base_projeto
        yield '        expires 30d;'
        yield '    }'
        yield ''
        yield '    location / {'
        yield '        include uwsgi_params;'
        yield '        uwsgi_pass %s:%s;' % (self.ip, self.porta)
        yield '    }'
        yield '}'

    def __cria_conf_nginx_gunicorn(self):
        yield 'upstream %s_socket_server {' % self.projeto
        yield '    server unix:%s/run/gunicorn.sock fail_timeout=0;' % self.base_projeto
        yield '}'
        yield ''
        yield 'server {'
        yield '    listen 80;'
        yield '    server_name %s;' % self.dominios
        yield '    access_log %s/logs/access.log;' % self.base_projeto
        yield '    error_log %s/logs/error.log;' % self.base_projeto
        yield ''
        yield '    location /static/ { # STATIC_URL'
        yield '        alias %s/static/; # STATIC_ROOT' % pjoin(self.diretorios['src'], self.projeto)
        yield '        expires 30d;'
        yield '    }'
        yield ''
        yield '    location /media/ { # MEDIA_URL'
        yield '        alias %s/media/; # MEDIA_ROOT' % pjoin(self.diretorios['src'], self.projeto)
        yield '        expires 30d;'
        yield '    }'
        yield ''
        yield '    location / {'
        yield '        proxy_pass http://%s_socket_server;' % self.projeto
        yield '        proxy_set_header Host $host;'
        yield '        proxy_set_header X-Real-IP $remote_addr;'
        yield '        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;'
        yield '    }'
        yield '}'

    def __cria_bin_gunicorn_start(self):
        yield '#!/usr/bin/env bash'
        yield 'NAME="%s"' % self.projeto
        yield 'DJANGODIR=%s' % pjoin(self.diretorios['src'], self.projeto)
        yield 'SOCKFILE=%s/run/gunicorn.sock' % self.base_projeto
        yield 'USER=root'
        yield 'GROUP=root'
        yield 'NUM_WORKERS=3'
        yield 'DJANGO_SETTINGS_MODULE=%s.settings' % self.projeto
        yield 'DJANGO_WSGI_MODULE=%s.wsgi' % self.projeto
        yield ''
        yield 'echo "Starting $NAME as `whoami`"'
        yield ''
        yield '# Activate the virtual environment'
        yield 'cd $DJANGODIR'
        yield 'source %s/bin/activate' % self.diretorios['venv']
        yield 'export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE'
        yield 'export PYTHONPATH=$DJANGODIR:$PYTHONPATH'
        yield ''
        yield '# Create the run directory if it doesnt exist'
        yield 'RUNDIR=$(dirname $SOCKFILE)'
        yield 'test -d $RUNDIR || mkdir -p $RUNDIR'
        yield ''
        yield '# Start your Django Unicorn'
        yield '# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)'
        yield 'exec %s/bin/gunicorn ${DJANGO_WSGI_MODULE}:application \\' % self.diretorios['venv']
        yield '  --name $NAME \\'
        yield '  --workers $NUM_WORKERS \\'
        yield '  --user=$USER --group=$GROUP \\'
        yield '  --log-level=debug \\'
        yield '  --bind=unix:$SOCKFILE'


    def __cria_bin_celeryd_start(self):
        yield '#!/usr/bin/env bash'
        yield 'USER=root'
        yield 'GROUP=root'
        yield ''
        yield 'echo "Starting $NAME as `whoami`"'
        yield ''
        yield '# Activate the virtual environment'
        yield 'cd $DJANGODIR'
        yield 'source %s/bin/activate' % self.diretorios['venv']
        yield ''
        yield '# Start your Django Celeryd'
        yield '# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)'
        yield 'exec %s/%s/manage.py celeryd -B -l INFO' % (self.diretorios['src'], self.projeto)


    def __cria_conf_supervisor(self):
        yield '[program:%s]' % self.projeto
        yield 'command = %s/bin/gunicorn_start' % self.diretorios['venv']
        yield 'user = root'
        yield 'autostart = True'
        yield 'autorestart = True'
        yield 'stdout_logfile = %s/stdout_gunicorn.log' % self.diretorios['logs']
        yield 'stderr_logfile = %s/stderr_gunicorn.log' % self.diretorios['logs']
        yield 'redirect_stderr = true'


    def __cria_conf_supervisor_celeryd(self):
        yield '[program:%s_celeryd]' % self.projeto
        yield 'command = %s/bin/celeryd_start' % self.diretorios['venv']
        yield 'user = root'
        yield 'autostart = True'
        yield 'autorestart = True'
        yield 'stdout_logfile = %s/stdout_celeryd.log' % self.diretorios['logs']
        yield 'stderr_logfile = %s/stderr_celeryd.log' % self.diretorios['logs']
        yield 'redirect_stderr = true'

    def __cria_conf_supervisor_gunicorn(self):
        d_venv = self.diretorios['venv']
        d_projeto = pjoin(self.diretorios['src'], self.projeto)
        ip = self.ip
        porta = self.porta
        yield '[program:%s]' % self.projeto
        yield 'command=%s/bin/python %s/manage.py run_gunicorn -b %s:%s' % (d_venv, d_projeto, ip, porta)
        yield 'directory=%s' % pjoin(self.diretorios['src'], self.projeto)
        yield 'user=root'
        yield 'autostart=True'
        yield 'autorestart=True'
        yield 'log_stderr=True'
        yield 'logfile= %s/supervisor.log' % self.diretorios['logs']

    def __cria_ambiente(self):
        self.__cria_diretorios()
        self.__cria_virtualenv()

        arquivo = self.confs['nginx']
        sys.stdout.write('Criando o arquivo "%s": ' % arquivo)
        with open(arquivo, 'w+') as arq:
            for s in self.__cria_conf_nginx_gunicorn():
                arq.write('%s\n' % s)
            arq.close()
        sys.stdout.write('[OK]\n')

        arquivo = self.confs['supervisor']
        sys.stdout.write('Criando o arquivo "%s": ' % arquivo)
        with open(arquivo, 'w+') as arq:
            for s in self.__cria_conf_supervisor():
                arq.write('%s\n' % s)
            arq.close()
        sys.stdout.write('[OK]\n')

        arquivo = self.confs['celeryd']
        sys.stdout.write('Criando o arquivo "%s": ' % arquivo)
        with open(arquivo, 'w+') as arq:
            for s in self.__cria_conf_supervisor_celeryd():
                arq.write('%s\n' % s)
            arq.close()
        sys.stdout.write('[OK]\n')

        arquivo = pjoin(self.diretorios['venv'], 'bin/gunicorn_start')
        sys.stdout.write('Criando o arquivo "%s": ' % arquivo)
        with open(arquivo, 'w+') as arq:
            for s in self.__cria_bin_gunicorn_start():
                arq.write('%s\n' % s)
            arq.close()
        sys.stdout.write('[OK]\n')

        os.chmod(arquivo, 0755)

        arquivo = pjoin(self.diretorios['venv'], 'bin/celeryd_start')
        sys.stdout.write('Criando o arquivo "%s": ' % arquivo)
        with open(arquivo, 'w+') as arq:
            for s in self.__cria_bin_celeryd_start():
                arq.write('%s\n' % s)
            arq.close()
        sys.stdout.write('[OK]\n')

        os.chmod(arquivo,0755)

    def run(self):
        self.__argumentos()
        self.__define_ambiente()
        self.__cria_ambiente()

        if self.atualizar:
            print('Não implementado.')
        else:
            self.__imprime_configuracoes()

if __name__ == '__main__':
    try:
        NginxDjangoGunicorn().run()
    except KeyboardInterrupt:
        print('\nFechado com Ctrl+C\n')
