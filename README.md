## NginxDjango

Programa escrito em python para criação de ambientes web para hospedagem de 
aplicações feitas em Django utilizando Gunicorn e Nginx.

root@debiandev:~# nginxDjangoGunicorn.py
usage: nginxDjangoGunicorn.py [-h] --projeto PROJETO --dominio DOMINIO
                              [--dominios DOMINIOS [DOMINIOS ...]] [--ip IP]
                              [--porta PORTA] [--atualizar]
                              [--caminho CAMINHO] [--proteger] [--remover]
                              [--remover-forcado]
